export default () => ({
    isLoading: true,
    entries: [
        {
            id: new Date().getTime(),
            date: new Date().toDateString(),
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent at quam id diam tempor dignissim. Pellentesque fringilla placerat tristique. Ut eu ante sit amet justo porttitor finibus. Vivamus in turpis ut libero efficitur  porttitor sed vitae ligula.',
            picture: null
        },
        {
            id: new Date().getTime() + 1000,
            date: new Date().toDateString(),
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent at quam id diam tempor dignissim. Pellentesque fringilla placerat tristique. Ut eu ante sit amet justo porttitor finibus. Vivamus in turpis ut libero efficitur  porttitor sed vitae ligula.',
            picture: null
        },
        {
            id: new Date().getTime() + 2000,
            date: new Date().toDateString(),
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent at quam id diam tempor dignissim. Pellentesque fringilla placerat tristique. Ut eu ante sit amet justo porttitor finibus. Vivamus in turpis ut libero efficitur  porttitor sed vitae ligula.',
            picture: null
        },
    ]
})
