export default {
    name:'daybook',
    component: () => import(/* webpackChunkName: "DayBook" */ '@/modules/daybook/layouts/DayBookLayout'),
    children:[
        {
            path:'',
            name:'daybook-no-entry',
            component: () => import(/* webpackChunkName: "NoEntry" */ '@/modules/daybook/views/NoEntrySelected'),
        },
        {
            path:':id',
            name:'daybook-entry',
            component: () => import(/* webpackChunkName: "NoEntry" */ '@/modules/daybook/views/EntryView'),
        },
    ]
}